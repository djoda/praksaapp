﻿using System;
using System.Collections.Generic;
using System.Text;
using PRAKSAApp.BLL.DTOs;
using PRAKSAApp.DAL.Entities;
using PRAKSAApp.DAL.Repositories;

namespace PRAKSAApp.BLL.Managers
{
    public class UserManager
    {
        private UserRepository userRepository = new UserRepository();
        
        public User Create(string username)
        {
            return userRepository.Create(username);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            List<UserDTO> dto = new List<UserDTO>();
            foreach (User u in userRepository.GetAll())
            {
                dto.Add(new UserDTO { Id = u.ID, Username = u.Username });
            }

            return dto;
        }

        public UserDTO Get(int id)
        {
            User user = userRepository.Get(id);
            return new UserDTO {Id=user.ID,Username=user.Username } ;
        }

        public User Update(int id,string username)
        {
            return userRepository.Update(id,username);
        }

        public void Delete(int id)
        {
            userRepository.Delete(id);
        }
    }
}
