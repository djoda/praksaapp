﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PRAKSAApp.BLL.Managers;
using PRAKSAApp.BLL.DTOs;

namespace PRAKSAApp.Presentation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private UserManager userManager = new UserManager();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IEnumerable<UserDTO> GetAll() {
            return userManager.GetAll();
        }
    }
}