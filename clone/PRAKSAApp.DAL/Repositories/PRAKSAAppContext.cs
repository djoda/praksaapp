﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PRAKSAApp.DAL.Entities;

namespace PRAKSAApp.DAL.Repositories
{
    public class PRAKSAAppContext : DbContext
    {
      /*public PRAKSAAppContext(DbContextOptions<PRAKSAAppContext> options) : base(options)
        {
        }*/

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

          //  modelBuilder.Entity<User>().HasData(new User {ID=1, Username = "Djoda" },new User {ID=2, Username="132"});
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=Core;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}
