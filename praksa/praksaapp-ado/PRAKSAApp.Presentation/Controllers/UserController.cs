﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PRAKSAApp.BLL.Contracts.DTOs;
using PRAKSAApp.BLL.Contracts.Managers;

namespace PRAKSAApp.Presentation.Controllers
{
    public class UserController : Controller
    {
        private IUserManager userManager;

        public UserController(IUserManager userManager) {
            this.userManager = userManager; 
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Update(int id)
        {
            JsonResult res = this.Get(id);
            return View(res.Value);
        }

        [HttpPost]
        public JsonResult Update(int id, string username) {
            return Json(userManager.Update(id, username));
        }

        [HttpPost]
        public JsonResult Create(string username)
        {
            return Json(userManager.Create(username));
        }

        [HttpPost]
        public void Delete(int id) {
            userManager.Delete(id);
        }

        public IEnumerable<UserDTO> GetAll() {
            return userManager.GetAll();
        }

        [HttpPost]
        public JsonResult Get(int id)
        {
            return Json(userManager.Get(id));
        }
    }
}