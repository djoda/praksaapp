﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PRAKSAApp.Presentation.Models
{
    public class UserRequest
    {
        public string Username { get; set; }
    }
}
