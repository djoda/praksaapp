﻿$('document').ready(function () {

    $.ajax({
        url: 'User/GetAll',
        type: 'GET',
        success: function (result) {
           console.log(result);
            let divUsers = document.getElementById('users');
            for (let user of result) {
                console.log(user);
                let tr = $('<tr asp-action="Update" id="' + user.id + '" scope="row"></tr>');

                tr.click(function () {
                    window.location.href = "user/update/?id=" + user.id;
                });

                tr.append('<td>' + user.id + '</td>');
                tr.append('<td>' + user.username + '</td>');
                let btn = $('<button class="btn btn-light">Delete</button>');
                btn.click(function (e) {
                    let parent = e.target.parentElement;
                    let td1 = parent.firstElementChild;
                    e.stopPropagation();
                    $.ajax({
                        url: 'user/delete',
                        type:"POST",
                        data: {id:user.id},
                        success: function () {
                            $('#' + user.id).remove();
                           
                        },
                        error: function () { }
                    });
                });
                let td3 = $('<td></td>');
                td3.append(btn);
                tr.append(td3);
                $('#forUsers').append(tr);
            }
        },
        error: function () { }
    });
    
    $('#createForm').submit(function (e) {
        e.preventDefault();
        let username = document.getElementsByName('username')[0];
        if (username.value !== "") {
            console.log(username.value);
            let data = { Username: username.value }
           
            $.ajax({
                url: 'create',
                type: "POST",
                data: { username: username.value},
                success: function () { },
                error: function () { }
            });
            
        }
    });

    $('#btnGetOne').click(function () {
        let getOne = $('input[name=getOneUser]');
        let idGet = getOne.val();
        console.log(idGet);
        $.ajax({
            url: 'user/get',
            type: 'POST',
            data: { id: idGet },
            success: function (res) {
                console.log(res);
                $('#getOne').append($('<p>Username: ' + res.username + '</p>'));
            },
            error: function () {}
        });
    });

    $('#btnUpdateBack').click(function () {
        var path = '@Url.Action("Index", "User")';
        $(this).load(path);
    });
});
