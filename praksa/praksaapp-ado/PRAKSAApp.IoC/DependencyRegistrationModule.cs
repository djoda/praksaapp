﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;
using PRAKSAApp.BLL.Managers;
using PRAKSAApp.DAL.ADO.Providers;

namespace PRAKSAApp.IoC
{
    public class DependencyRegistrationModule
    {
        public void RegisterDependency(ContainerBuilder builder)
        {
            builder.RegisterType<UserManager>().AsImplementedInterfaces();
            builder.RegisterType<UserProvider>().AsImplementedInterfaces();
        }
    }
}
