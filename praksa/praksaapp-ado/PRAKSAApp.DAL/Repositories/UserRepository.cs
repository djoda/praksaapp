﻿using System;
using System.Collections.Generic;
using System.Text;
using PRAKSAApp.DAL.Entities;
using System.Linq;

namespace PRAKSAApp.DAL.Repositories
{
   
    public class UserRepository
    {
        private PRAKSAAppContext _context = new PRAKSAAppContext();

        public User Create(string username) {
            User s = new User {Username = username };
            _context.Users.Add(s);
            _context.SaveChanges();
            return _context.Users.Where(e => e.ID == s.ID).FirstOrDefault();
        }

        public IEnumerable<User> GetAll() {
            return _context.Users;
        }

        public User Get(int id) {
            return _context.Users.First(e => e.ID == id);
        }

        public User Update(int id, string username)
        {
            User found = _context.Users.First(e => e.ID == id);
            found.Username = username;

            _context.Users.Update(found);
            _context.SaveChanges();

            return found;
        }

        public void Delete(int id)
        {
            _context.Users.Remove(_context.Users.First(e => e.ID == id));
            _context.SaveChanges();
        }
    }
}
