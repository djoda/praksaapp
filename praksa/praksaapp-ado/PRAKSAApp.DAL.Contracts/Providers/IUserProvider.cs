﻿using System;
using System.Collections.Generic;
using System.Text;
using PRAKSAApp.DAL.Contracts.Entities;

namespace PRAKSAApp.DAL.Contracts.Providers
{
    public interface IUserProvider
    {
        IEnumerable<User> GetAll();

        User Get(int id);

        User Create(string username);

        void Delete(int id);

        User Update(int id, string usename);
    }
}
