﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using PRAKSAApp.DAL.Contracts.Entities;
using PRAKSAApp.DAL.Contracts.Providers;

namespace PRAKSAApp.DAL.ADO.Providers
{
    public class UserProvider : IUserProvider
    {
        private readonly string ConnectionInformation = "Server=(localdb)\\MSSQLLocalDB;Database=praksa;Trusted_Connection=True;MultipleActiveResultSets=true";
        SqlDataReader myReader;

        /*public UserProvider(IServiceCollection services)
        {
            services.AddScoped<IUserProvider, UserProvider>();
        }*/

        

        public IEnumerable<User> GetAll()
        {
            List<User> users = new List<User>();
            SqlConnection mainConnection = new SqlConnection(ConnectionInformation);
            mainConnection.Open();
            SqlCommand command = new SqlCommand("Select * from Users", mainConnection);
            
            myReader = command.ExecuteReader();

            while (myReader.Read())
            {
                users.Add(new User { ID = (int)myReader.GetValue(0), Username = myReader.GetValue(1).ToString() });
            }
            
            mainConnection.Close();
            return users;
        }

        public User Get(int id)
        {
            User user = new User();
            SqlConnection mainConnection = new SqlConnection(ConnectionInformation);
            mainConnection.Open();
            SqlCommand command = new SqlCommand("Select * from Users where ID = '"+ id +"'", mainConnection);
                        
            myReader = command.ExecuteReader();
            if (myReader.HasRows)
            {
                myReader.Read();
                user = new User { ID = (int)myReader.GetValue(0), Username = myReader.GetValue(1).ToString() };
            }
            mainConnection.Close();
            return user;
        }

        public User Create(string username)
        {
            SqlConnection mainConnection = new SqlConnection(ConnectionInformation);
            mainConnection.Open();
            SqlCommand command = new SqlCommand("Insert into Users (Username) values ('"+username+"')", mainConnection);
            command.ExecuteNonQuery();
            SqlCommand command2 = new SqlCommand("Select ID from Users ORDER BY ID DESC",mainConnection);
            
            User user = this.Get((int)command2.ExecuteScalar());
            mainConnection.Close();
            return user;
        }

        public void Delete(int id) {
            SqlConnection mainConnection = new SqlConnection(ConnectionInformation);
            mainConnection.Open();
            SqlCommand command = new SqlCommand("Delete from Users where ID = '" + id.ToString() +"'",mainConnection);
            command.ExecuteNonQuery();
            mainConnection.Close();
        }

        public User Update(int id, string usename) {
            SqlConnection mainConnection = new SqlConnection(ConnectionInformation);
            mainConnection.Open();
            SqlCommand command = new SqlCommand("Update Users set Username  = '" + usename + "' where ID = '"+ id.ToString() +"'",mainConnection);
            command.ExecuteScalar();
            mainConnection.Close();
            User user = this.Get(id);
            return user;
        }
    }
}
