﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using PRAKSAApp.BLL.Contracts.Managers;
using PRAKSAApp.BLL.Contracts.DTOs;
using PRAKSAApp.DAL.Contracts.Providers;
using PRAKSAApp.DAL.Contracts.Entities;

namespace PRAKSAApp.BLL.Managers
{
    public class UserManager: IUserManager
    {
        private IUserProvider userProvider;

        public UserManager(IUserProvider userProvider)
        {
            this.userProvider = userProvider;
        }
        
        public UserDTO Create(string username)
        {
            User user = userProvider.Create(username);
            return new UserDTO { Id = user.ID, Username = user.Username };
        }

        public IEnumerable<UserDTO> GetAll()
        {
            List<UserDTO> dto = new List<UserDTO>();
            foreach (User u in userProvider.GetAll())
            {
                dto.Add(new UserDTO { Id = u.ID, Username = u.Username });
            }
            return dto;
        }

        public UserDTO Get(int id)
        {
            User user = userProvider.Get(id);
            return new UserDTO {Id=user.ID,Username=user.Username } ;
        }

        public UserDTO Update(int id,string username)
        {
            User user = userProvider.Update(id,username);
            return new UserDTO { Id = user.ID, Username = user.Username };
        }

        public void Delete(int id)
        {
            userProvider.Delete(id);
        }
    }
}
