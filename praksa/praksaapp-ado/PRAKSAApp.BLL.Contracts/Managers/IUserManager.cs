﻿using System;
using System.Collections.Generic;
using System.Text;
using PRAKSAApp.BLL.Contracts.DTOs;

namespace PRAKSAApp.BLL.Contracts.Managers
{
    public interface IUserManager
    {
        UserDTO Create(string username);

        IEnumerable<UserDTO> GetAll();

        UserDTO Get(int id);

        UserDTO Update(int id, string username);

        void Delete(int id);
    }
}
