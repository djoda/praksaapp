﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PRAKSAApp.BLL.Contracts.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}
